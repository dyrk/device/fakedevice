from python:3.9

WORKDIR /app

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

COPY device.py .
COPY deviceconfig.yml .

CMD [ "python3", "device.py"]