import logging
import threading
import time
import os
from random import randint

from dyrkdevice.basicDyrkDevice import BasicDyrkDevice

device = BasicDyrkDevice()

if "clientid" in os.environ:
    device.device_config['mqtt']['broker']['client_id'] = os.environ.get('clientid')
    device.device_config['mqtt']['broker']['username'] = os.environ.get('clientid')

if "mqqt_host" in os.environ:
    device.device_config['mqtt']['broker']['endpoint'] = os.environ.get('mqqt_host')

if "mqqt_port" in os.environ:
    device.device_config['mqtt']['broker']['port'] = os.environ.get('mqqt_port')

if "mqqt_usr" in os.environ:
    device.device_config['mqtt']['broker']['username'] = os.environ.get('mqqt_usr')

if "mqqt_pwd" in os.environ:
    device.device_config['mqtt']['broker']['password'] = os.environ.get('mqqt_pwd')


def time_function(pin, timeout):
    logging.info(f"Turning pin {pin} on for {timeout} seconds")
    logging.info(f"Pin {pin} on")
    time.sleep(timeout)
    logging.info(f"Pin {pin} off")


@device.event(eventName="output_event")
def water(output_state: list):
    threads = []
    for i, value in enumerate(output_state):
        threads.append(threading.Thread(target=time_function, args=(i, value)))

    for thread in threads:
        thread.start()

    for thread in threads:
        thread.join()


@device.measure(measureName="FAKE", interval=5)
def FAKE():
    data_event = {
        "event_type": "data_event",
        "timestamp": int(time.time()),
        "data": {
            "temperature": randint(10, 30),
            "pressure": randint(0, 100),
            "humidity": randint(0, 100),
        }
    }
    return data_event

@device.event(eventName="data_event")
def data(_):
    pass

device.run()